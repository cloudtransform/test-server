const http = require('http');
var bodyParser = require("body-parser");
var express = require("express")
var { Pool } = require('pg');

// JWT, login, sign in, complete challenge,

var app = express()

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.post("/api/question", (req, res, next) => {
	const questionId = req.body.questionId;
    res.json({"answerId": questionId})
});

app.get("/api", (req, res, next) => {
	const username = req.body.username;
    res.json({"message":"Welcome to server"})
});

// Default response for any other request
app.use(function(req, res){
    res.status(404);
    res.json({"error":"oopsies"});
});

process.on('uncaughtException', function (error) {
   console.log(error.stack);
});

// Start server
let port = process.env.PORT;
if (port == null || port == "") {
  port = 8080;
}
app.listen(port);