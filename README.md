# Test server
This application was build to test the commmunication between pods when and the update of a cluster.

## Creating the image
Run the following command:

````shell script
docker build . -t node-server
````

This will create an image called node-server.



## Running the node server on a kubernetes cluster
Run the following command:

````shell script
kubectl apply -f deployment.yaml
````
This command will pull the geert263/node-server image from docker hub.
You can change this in the deployment.yaml to your own image.